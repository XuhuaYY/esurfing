import requests
import configparser
import time
import hashlib
from dataclasses import dataclass

import socket
import uuid

CONFIG_PATH = "./config.ini"
SECERT = "Eshore!@#"
ISWIFI = "4060"
SESSION = requests.session()
SESSION.headers[
    "User-Agent"
] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36"
NASIP = "119.146.175.80"



def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    ip = s.getsockname()[0]

    ip2=str(ip).split("\n")[0]
    return ip2

IP = get_ip_address()
print(f"本地IP地址为：{IP}")

def get_mac_address():
    mac = uuid.UUID(int = uuid.getnode()).hex[-12:]
    ic = ":".join([mac[e: e+2] for e in range(0, 11, 2)])

    ic=str(ic).split("\n")[0]
    ic=ic.replace(":","-")
    return ic.upper()

MAC = get_mac_address()
print(f"本地MAC地址为：{MAC}")



@dataclass
class Config:
    account: str
    password: str


def print_error_and_exit(data: str):
    print(f"ERROR: {data}")
    exit(-1)


def read_config(path: str) -> Config:
    config = configparser.ConfigParser()
    config.read(path)
    config_object = Config(
        config["record"]["account"],
        config["record"]["password"],
    )

    return config_object


def auth():
    response = SESSION.get(
        "http://172.17.18.2:8080/byod/templatePage/20200318110733542/guestRegister.jsf",
        timeout=3,
    )
    cookies = response.headers.get("Set-Cookie", "")
    if cookies == "":
        print_error_and_exit("没有获取到认证cookie！")
    print("获取认证cookie成功，正在激活cookie...")
    response = SESSION.get(
        "http://172.17.18.2:8080/byod/index.xhtml", timeout=3
    )
    print(response.text)
    auth_text = input('请输入 id="javax.faces.ViewState" value="XXX" 的内容：')
    response = SESSION.post(
        "http://172.17.18.2:8080/byod/index.xhtml",
        allow_redirects=True,
        timeout=3,
    )
    print(response.headers)


def get_md5_str(s: str) -> str:
    return hashlib.md5(s.encode("utf-8")).hexdigest().upper()


def get_verify_code(config: Config) -> str:
    url = "http://enet.10000.gd.cn:10001/client/challenge"
    time_stamp = str(time.time()).replace(".", "")[:13]
    md5_string = get_md5_str(
        IP + NASIP + MAC + time_stamp + SECERT
    )
    data = {
        "username": config.account,
        "clientip": IP,
        "nasip": NASIP,
        "mac": MAC,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Host": "enet.10000.gd.cn:10001",
    }
    response = SESSION.post(url, json=data, headers=headers, timeout=3)
    verify_code = response.json()["challenge"]
    return verify_code


def login(verify_code: str) -> str:
    url = "http://enet.10000.gd.cn:10001/client/login"
    time_stamp = str(time.time()).replace(".", "")[:13]
    md5_string = get_md5_str(
        IP
        + NASIP
        + MAC
        + time_stamp
        + verify_code
        + SECERT
    )
    data = {
        "username": config.account,
        "password": config.password,
        "clientip": IP,
        "nasip": NASIP,
        "mac": MAC,
        "iswifi": ISWIFI,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Host": "enet.10000.gd.cn:10001",
    }
    response = SESSION.post(url, json=data, headers=headers, timeout=3)

    return response.text


def keep():
    url = "http://enet.10000.gd.cn:8001/hbservice/client/active"
    time_stamp = str(time.time()).replace(".", "")[:13]
    md5_string = get_md5_str(
        IP
        + NASIP
        + MAC
        + time_stamp
        + SECERT
    )
    payload = {
        "username": config.account,
        "clientip": IP,
        "nasip": NASIP,
        "mac": MAC,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    try:
        SESSION.post(url, params=payload, timeout=3)
        print("持续成功...")
    except Exception as e:
        try:
            print("连接失败,正在请求重新连接")
            verify_code = get_verify_code(config)
            print(f"获取验证码为：{verify_code}")
            login_response = login(verify_code)
            print(f"登录返回的信息为：{login_response}")
        except:
            print("网络异常...")


if __name__ == "__main__":
    config = read_config(CONFIG_PATH)
    try:
        verify_code = get_verify_code(config)
        print(f"获取验证码为：{verify_code}")
        login_response = login(verify_code)
        print(f"登录返回的信息为：{login_response}")
        if "success" in login_response:
            print("登录成功！接下来将维持连接")
    except:
        print("请检查是否正常连接天翼校园！")
    while True:
        time.sleep(10)
        keep()
